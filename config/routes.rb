# -*- encoding : utf-8 -*-
Gesco::Application.routes.draw do 
  resources :rolagents

  resources :attendants

  resources :meetings

  resources :docs

  resources :commissions

  resources :units

  resources :agents

  resources :values
	

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
   root :to => "user_sessions#new"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
  Gesco::Application.routes.draw do
    resources :agents, :user_sessions, :rolagents, :attendants, :commissions
    match 'login' => 'user_sessions#new', :as => :login
    match 'logout' => 'user_sessions#destroy', :as => :logout
    match 'rolagents/search/:id' => 'rolagents#search'
    match 'attendants/search/:id' => 'attendants#search'
    match 'commissions/pdf/:id/:a' => 'commissions#pdf'
    match 'commissions/searchbyunit/:id' => 'commissions#searchbyunit'
    match 'rolagents/searchbycomm/:id' => 'rolagents#searchbycomm'
    match 'rolagents/searchbycomm/:id/:u' => 'rolagents#searchbycomm'
    match 'meetings/searchbycomm/:id' => 'meetings#searchbycomm'
    match 'docs/searchbycomm/:id' => 'docs#searchbycomm'
    match 'docs/searchbymeet/:id' => 'docs#searchbymeet'
    match 'commissions/new/:id' => 'commissions#new'
    match 'meetings/new/:id' => 'meetings#new'
    match 'rolagents/new/:id/:u' => 'rolagents#new'
    match 'docs/new/:id' => 'docs#new'
    match 'docs/new/:id' => 'docs#new'
  end

end
