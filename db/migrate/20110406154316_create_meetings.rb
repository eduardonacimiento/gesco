# -*- encoding : utf-8 -*-
class CreateMeetings < ActiveRecord::Migration
  def self.up
    create_table :meetings do |t|
      t.references :commission
      t.references :estado
      t.datetime :fechaconvocatoria
      t.datetime :fechainicio
      t.text :motivocancelacion
      t.text :ordendia
      t.text :acta
      t.text :fechaaprobacion
      t.references :doc

      t.timestamps
    end
  end

  def self.down
    drop_table :meetings
  end
end
