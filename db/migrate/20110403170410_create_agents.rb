# -*- encoding : utf-8 -*-
class CreateAgents < ActiveRecord::Migration
  def self.up
    create_table :agents do |t|
      t.references :tipo
      t.references :estado
      t.string :nombre
      t.string :apellido1
      t.string :apellido2
      t.references :tipodoc
      t.string :numdoc
      t.string :email1
      t.string :email2
      t.string :telefono1
      t.string :telefono2
      t.references :tipoauth
      t.string :usuario
      t.string :password
      t.string :serverimap
      t.string :serverldap
      t.string :baseldap
      t.references :rolagent

      t.timestamps
    end
  end

  def self.down
    drop_table :agents
  end
end
