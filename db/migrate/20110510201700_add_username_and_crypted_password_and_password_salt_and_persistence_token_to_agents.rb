# -*- encoding : utf-8 -*-
class AddUsernameAndCryptedPasswordAndPasswordSaltAndPersistenceTokenToAgents < ActiveRecord::Migration
  def self.up
    add_column :agents, :username, :string
    add_column :agents, :crypted_password, :string
    add_column :agents, :password_salt, :string
    add_column :agents, :persistence_token, :string
  end

  def self.down
    remove_column :agents, :persistence_token
    remove_column :agents, :password_salt
    remove_column :agents, :crypted_password
    remove_column :agents, :username
  end
end
