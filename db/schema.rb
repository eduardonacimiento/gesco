# -*- encoding : utf-8 -*-
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110517110722) do

  create_table "agents", :force => true do |t|
    t.integer  "tipo_id"
    t.integer  "estado_id"
    t.string   "nombre"
    t.string   "apellido1"
    t.string   "apellido2"
    t.integer  "tipodoc_id"
    t.string   "numdoc"
    t.string   "email1"
    t.string   "email2"
    t.string   "telefono1"
    t.string   "telefono2"
    t.integer  "tipoauth_id"
    t.string   "usuario"
    t.string   "password"
    t.string   "serverimap"
    t.string   "serverldap"
    t.string   "baseldap"
    t.integer  "rolagent_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.string   "rolagent"
  end

  create_table "attendants", :force => true do |t|
    t.integer  "meeting_id"
    t.integer  "agent_id"
    t.integer  "estado_id"
    t.text     "description"
    t.integer  "ausencia_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "commissions", :force => true do |t|
    t.integer  "tipo_id"
    t.integer  "estado_id"
    t.integer  "unidad_id"
    t.string   "nombre"
    t.text     "nombrelargo"
    t.text     "funciones"
    t.datetime "fechacreacion"
    t.datetime "fechabaja"
    t.text     "motivobaja"
    t.text     "composicion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "visibilidad_id"
  end

  create_table "docs", :force => true do |t|
    t.integer  "commission_id"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meetings", :force => true do |t|
    t.integer  "commission_id"
    t.integer  "estado_id"
    t.datetime "fechaconvocatoria"
    t.datetime "fechainicio"
    t.text     "motivocancelacion"
    t.text     "ordendia"
    t.text     "acta"
    t.text     "fechaaprobacion"
    t.integer  "doc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "lugar"
  end

  create_table "rolagents", :force => true do |t|
    t.integer  "rol_id"
    t.integer  "agent_id"
    t.integer  "unit_id"
    t.integer  "commission_id"
    t.datetime "fechaalta"
    t.datetime "fechabaja"
    t.text     "motivobaja"
    t.integer  "estado_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "units", :force => true do |t|
    t.string   "nombre"
    t.text     "nombrelargo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "values", :force => true do |t|
    t.string   "codigo"
    t.text     "tabla"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
