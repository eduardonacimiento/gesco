# -*- encoding : utf-8 -*-
require 'test_helper'

class RolagentsControllerTest < ActionController::TestCase
  setup do
    @rolagent = rolagents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:rolagents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create rolagent" do
    assert_difference('Rolagent.count') do
      post :create, :rolagent => @rolagent.attributes
    end

    assert_redirected_to rolagent_path(assigns(:rolagent))
  end

  test "should show rolagent" do
    get :show, :id => @rolagent.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @rolagent.to_param
    assert_response :success
  end

  test "should update rolagent" do
    put :update, :id => @rolagent.to_param, :rolagent => @rolagent.attributes
    assert_redirected_to rolagent_path(assigns(:rolagent))
  end

  test "should destroy rolagent" do
    assert_difference('Rolagent.count', -1) do
      delete :destroy, :id => @rolagent.to_param
    end

    assert_redirected_to rolagents_path
  end
end
