# -*- encoding : utf-8 -*-

class ApplicationController < ActionController::Base

  protect_from_forgery
  
  helper_method :current_user
  helper_method :is_admin
  helper_method :is_admin_UO
  helper_method :is_secre
  helper_method :is_vocal
  helper_method :is_anony
  helper_method :get_bread_crumb
  helper_method :traduce
  
  private
  
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end
  
  def is_admin
    unless current_user.nil? then
      @rol_current_user = Rolagent.find(:all, :conditions => ['agent_id like ?', current_user.id]).map { |x| x.rol.description}# + '|' + x.unit.nombre + '|' + x.commission.nombre}
      @current_user.rolagent = @rol_current_user
      unless current_user.rolagent.nil?
        current_user.rolagent.each do |tmp|
          if tmp.split("|").include?('Administrador') then
            return true
          end
        end        
      end
    end
    return false
  end
  
  def is_admin_UO
    unless current_user.nil? then
      @rol_current_user = Rolagent.find(:all, :conditions => ['agent_id like ?', current_user.id]).map { |x| x.rol.description}# + '|' + x.unit.nombre + '|' + x.commission.nombre}
      @current_user.rolagent = @rol_current_user
      unless current_user.rolagent.nil?
        current_user.rolagent.each do |tmp|
          if tmp.split("|").include?('Administrador unidad organizativa') then
            return true
          end
        end        
      end
    end
    return false
  end
  
  def is_secre
    unless current_user.nil? then
      @rol_current_user = Rolagent.find(:all, :conditions => ['agent_id like ?', current_user.id]).map { |x| x.rol.description}# + '|' + x.unit.nombre + '|' + x.commission.nombre}
      @current_user.rolagent = @rol_current_user
      unless current_user.rolagent.nil?
        current_user.rolagent.each do |tmp|
          if tmp.split("|").include?('Secretario') then
            return true
          end
        end        
      end
    end
    return false
  end
  
  def is_vocal
    unless current_user.nil? then
      @rol_current_user = Rolagent.find(:all, :conditions => ['agent_id like ?', current_user.id]).map { |x| x.rol.description}# + '|' + x.unit.nombre + '|' + x.commission.nombre}
      @current_user.rolagent = @rol_current_user
      unless current_user.rolagent.nil?
        current_user.rolagent.each do |tmp|
          if tmp.split("|").include?('Vocal') then
            return true
          end
        end        
      end
    end
    return false
  end
  
  def is_anony
    unless current_user.nil? then
      @rol_current_user = Rolagent.find(:all, :conditions => ['agent_id like ?', current_user.id]).map { |x| x.rol.description}# + '|' + x.unit.nombre + '|' + x.commission.nombre}
      @current_user.rolagent = @rol_current_user
      unless current_user.rolagent.nil?
        current_user.rolagent.each do |tmp|
          if tmp.split("|").include?('Miembro') then
            return true
          end
        end        
      end
    end
    return false
  end
  
  def redirect_to_login
    redirect_to :login
  end
  
  
  def render_500(exception = nil)
    if exception
      logger.info "Rendering 500 with exception: #{exception.message}"
    end
  
    respond_to do |format|
      format.html { render :file => "/public/500.html", :status => :forbidden }
      #format.html { render :nothing => true, :status => :forbidden } #:file => "#{Rails.root}/public/500.html", 
      format.xml  { head :forbidden }
      format.any  { head :forbidden }
    end
  end

def get_bread_crumb
	url = request.request_uri
	breadcrumbs = []
	elements = url.split('/')
	last_element = elements.last
	elements.each do |element| 
		breadcrumbs << element 
	end
	return breadcrumbs
end  

def traduce(element)
	element = element.gsub('commissions', 'Comisiones') 
	element = element.gsub('units', 'Unidades')
	element = element.gsub('meetings', 'Reuniones')
	element = element.gsub('attendants', 'Asistentes')
	element = element.gsub('values', 'Tabla de valores')
	element = element.gsub('rolagents', 'Roles de Usuarios')
	element = element.gsub('agents', 'Usuarios')
	element = element.gsub('edit', 'Editar')
	element = element.gsub('new', 'Nuevo')
	return element
end
  
end
