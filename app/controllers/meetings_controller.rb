# -*- encoding : utf-8 -*-
class MeetingsController < ApplicationController
  # GET /meetings
  # GET /meetings.xml
  def index
    unless current_user.nil?
      @meetings = Meeting.all
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @meetings }
      end
    else
      redirect_to_login
    end
  end

  # GET /meetings/1
  # GET /meetings/1.xml
  def show
    unless current_user.nil?
      @meeting = Meeting.find(params[:id])
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @meeting }
      end
    else
      redirect_to_login
    end
  end
  
  def searchbycomm
     unless current_user.nil?
         if is_admin || is_admin_UO || is_secre
            @meetings = Meeting.find(:all, :conditions => {:commission_id => params[:id] })
         else
            @attendants = Attendant.find(:all, :conditions => {:agent_id => current_user.id }).map { |r| [r.meeting.id]}
            @meetings = Meeting.find(:all, :conditions => {:id => @attendants, :commission_id => params[:id] })
         end
         render :index
     else 
      redirect_to_login
     end
  end
  

  # GET /meetings/new
  # GET /meetings/new.xml
  def new
    unless current_user.nil?
      if is_admin || is_admin_UO  || is_secre
        @meeting = Meeting.new
        respond_to do |format|
          format.html # new.html.erb
          format.xml  { render :xml => @meeting }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # GET /meetings/1/edit
  def edit
    unless current_user.nil?
      if is_admin || is_admin_UO  || is_secre
        @meeting = Meeting.find(params[:id])
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # POST /meetings
  # POST /meetings.xml
  def create
    unless current_user.nil?
      if is_admin || is_admin_UO  || is_secre
        @meeting = Meeting.new(params[:meeting])
        respond_to do |format|
          if @meeting.save
            format.html { redirect_to(meetings_path + "/searchbycomm/" + @meeting.commission.id.to_s, :notice => 'Reunión creada satisfactoriamente.') }
            format.xml  { render :xml => @meeting, :status => :created, :location => @meeting }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @meeting.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # PUT /meetings/1
  # PUT /meetings/1.xml
  def update
    unless current_user.nil?
      if is_admin || is_admin_UO  || is_secre
        @meeting = Meeting.find(params[:id])
    
        respond_to do |format|
          if @meeting.update_attributes(params[:meeting])
            format.html { redirect_to(meetings_path + "/searchbycomm/" + @meeting.commission.id.to_s, :notice => 'Reunión editada satisfactoriamente.') }
            format.xml  { head :ok }
          else
            format.html { render :action => "edit" }
            format.xml  { render :xml => @meeting.errors, :status => :unprocessable_entity }
          end
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end

  # DELETE /meetings/1
  # DELETE /meetings/1.xml
  def destroy
    unless current_user.nil?
      if is_admin || is_admin_UO  || is_secre
        @meeting = Meeting.find(params[:id])
        @meeting.destroy
    
        respond_to do |format|
          format.html { redirect_to(:back) }
          format.xml  { head :ok }
        end
      else 
        render_500
      end
    else
      redirect_to_login
    end
  end
end
