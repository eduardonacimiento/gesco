# -*- encoding : utf-8 -*-
class DocsController < ApplicationController
  # GET /docs
  # GET /docs.xml
  def index
    @docs = Doc.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @docs }
    end
  end

  # GET /docs/1
  # GET /docs/1.xml
  def show
    @doc = Doc.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @doc }
    end
  end
  
  def searchbycomm
     unless current_user.nil?
        @searchbycomm = params[:id];
        @docs = Doc.find(:all, :conditions => {:commission_id => params[:id] })
        render :index
     else 
      redirect_to_login
     end
  end


  def searchbymeet
     unless current_user.nil?
	  	  @meet_docs = Meeting.find(:all, :conditions => {:id => params[:id]})
		  vacio = true
		  @meet_docs.each do |met|
		     unless met.doc.nil?
			     vacio = false
		     end
		  end
		  unless vacio
  		     @meet_docs = @meet_docs.map {|m| [m.doc.id]}  
   	     @docs = Doc.find(:all, :conditions => {:id => @meet_docs})
		  else
		  	 @docs = nil
		  end
        render :index
     else 
      redirect_to_login
     end
  end

  # GET /docs/new
  # GET /docs/new.xml
  def new
    @doc = Doc.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @doc }
    end
  end

  # GET /docs/1/edit
  def edit
    @doc = Doc.find(params[:id])
  end

  # POST /docs
  # POST /docs.xml
  def create
    @doc = Doc.new(params[:doc])
    tmp = @doc.url.tempfile
	 time = Time.new
	 dir = 'public/uploads/' + time.year.to_s + '/' + time.month.to_s + '/' + @doc.commission.nombre.gsub(' ', '').downcase + '/' + '.pdf'
	 FileUtils.mkdir_p(File.dirname(dir)) unless File.exists?(dir)
    file = File.join(Rails.root, "public", "uploads", time.year.to_s, time.month.to_s, @doc.commission.nombre.gsub(' ', '').downcase, @doc.url.original_filename)
    FileUtils.cp tmp.path, file
    FileUtils.rm tmp.path

  	 @doc.url = '/uploads/' + time.year.to_s + '/' + time.month.to_s + '/' + @doc.commission.nombre.gsub(' ', '').downcase + '/'  + @doc.url.original_filename
    respond_to do |format|
      if @doc.save
        format.html { redirect_to(docs_path + "/searchbycomm/" + @doc.commission.id.to_s, :notice => 'Documentación creada satisfactoriamente.') }
        format.xml  { render :xml => @doc, :status => :created, :location => @doc }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @doc.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /docs/1
  # PUT /docs/1.xml
  def update
    @doc = Doc.find(params[:id])

    respond_to do |format|
      if @doc.update_attributes(params[:doc])
        format.html { redirect_to(docs_path + "/searchbycomm/" + @doc.commission.id.to_s, :notice => 'Documentación editada satisfactoriamente.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @doc.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /docs/1
  # DELETE /docs/1.xml
  def destroy
    @doc = Doc.find(params[:id])
    @doc.destroy

    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end
end
