# -*- encoding : utf-8 -*-
class Doc < ActiveRecord::Base
  belongs_to :commission
  has_many :meetings
end
