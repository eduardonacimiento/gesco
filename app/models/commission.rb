# -*- encoding : utf-8 -*-
class Commission < ActiveRecord::Base
  belongs_to :tipo, :class_name => "Value"
  belongs_to :estado, :class_name => "Value"
  belongs_to :unidad, :class_name => "Unit"
  belongs_to :visibilidad, :class_name => "Value"
  has_many :docs
end
