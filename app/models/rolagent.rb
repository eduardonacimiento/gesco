# -*- encoding : utf-8 -*-
class Rolagent < ActiveRecord::Base
  belongs_to :rol, :class_name => "Value"
  belongs_to :agent
  belongs_to :unit
  belongs_to :commission
  belongs_to :estado, :class_name => "Value"
end
